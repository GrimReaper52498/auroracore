package com.grimreaper52498.auroracore.player;

import com.grimreaper52498.auroracore.nms.ActionBar;
import com.grimreaper52498.auroracore.nms.BossBarAPI;
import com.grimreaper52498.auroracore.nms.Tablist;
import com.grimreaper52498.auroracore.nms.Titles;
import com.grimreaper52498.auroracore.utils.ServerUtils;
import com.grimreaper52498.auroracore.utils.StringUtils;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * Created by GrimReaper52498 on 4/22/2016.
 *
 * @author GrimReaper52498 (Tyler Brady)
 */
public class AuroraPlayer {

    private Player player;

    public AuroraPlayer(Player player) {
        this.player = player;
    }


    /**
     * Send a message to the player, with color codes
     *
     * @param message message with colorsq          ``
     */
    public void sendMessage(String message) {
        message = StringUtils.color(message);
        if (player.isOnline()) {
            player.sendMessage(message);
        }
    }

    /**
     * Get the ping of the player
     *
     * @return The ping of the player
     * @throws ClassNotFoundException
     * @throws NoSuchMethodException
     * @throws InvocationTargetException
     * @throws IllegalAccessException
     * @throws NoSuchFieldException
     */
    public int getPing() throws ClassNotFoundException, NoSuchMethodException, InvocationTargetException, IllegalAccessException, NoSuchFieldException {

        Class<?> craftPlayer = Class.forName("org.bukkit.craftbukkit." + Bukkit.getServer().getClass().getPackage().getName().split("\\.")[3] + ".entity.CraftPlayer");
        Object converted = craftPlayer.cast(player);
        Method handle = converted.getClass().getMethod("getHandle");
        Object entityPlayer = handle.invoke(converted);
        Field pingField = entityPlayer.getClass().getField("ping");

        return pingField.getInt(entityPlayer);
    }

    /**
     * Send a message to the player using the Actionbar
     *
     * @param message message to send
     */
    public void sendActionbarMessage(String message) {
        ActionBar.sendAction(player, message);
    }

    /**
     * Set message in the players tablist header and footer with colors
     *
     * @param header text to set for header
     * @param footer text to set for footer
     */
    public void setTabHeaderFooter(String header, String footer) {
        Tablist.sendTablist(player, header, footer);
    }

    /**
     * Send player title and subtitle - Set Subtitle to null to not use
     *
     * @param title    title to send
     * @param subtitle subtitle to send if desired, null otherwise
     */
    public void sendTitleAndSubtitle(String title, String subtitle) {
        if (title != null) {
            if (ServerUtils.isPointEight()) {
                Titles titles = new Titles(title);
                if (subtitle != null) {
                    titles.setSubtitle(subtitle);
                }
                if (player.isOnline()) {
                    titles.send(player);
                }
            } else if (ServerUtils.isPointNine()) {
                player.sendTitle(StringUtils.color(title), (subtitle == null ? "" : StringUtils.color(subtitle)));
            }
        }
    }

    /**
     * Send a message to the player via the bossbar (1.8-)
     *
     * @param message Message to send
     * @param percent Percent of BossBar
     */
    public void sendBossBarMessage(String message, int percent) {
        try {
            BossBarAPI.getInstance().setStatus(player, message, percent, true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
