package com.grimreaper52498.auroracore.cache;

import com.grimreaper52498.auroracore.player.AuroraPlayer;
import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by GrimReaper52498 on 4/22/2016.
 *
 * @author GrimReaper52498 (Tyler Brady)
 */
public class PlayerCaches {

    private static Map<Player, AuroraPlayer> players = new HashMap<Player, AuroraPlayer>();

    public static Map<Player, AuroraPlayer> getPlayers(){
        return players;
    }

}
