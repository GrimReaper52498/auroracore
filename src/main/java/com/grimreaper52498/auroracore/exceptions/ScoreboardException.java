package com.grimreaper52498.auroracore.exceptions;

/**
 * Created by GrimReaper52498 on 4/21/2016.
 *
 * @author GrimReaper52498 (Tyler Brady)
 */
public class ScoreboardException extends Exception {

    public ScoreboardException(String message){
        super(message);
    }

}
