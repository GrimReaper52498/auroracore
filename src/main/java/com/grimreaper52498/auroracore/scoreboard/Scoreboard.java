package com.grimreaper52498.auroracore.scoreboard;

import com.grimreaper52498.auroracore.AuroraCore;
import com.grimreaper52498.auroracore.exceptions.ScoreboardException;
import com.grimreaper52498.auroracore.utils.logger.Logger;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;

import java.util.HashMap;
import java.util.Map;

/**
 * Class to easily create one or more Scoreboards and display them to players
 *
 * @author GrimReaper52498 (Tyler Brady)
 */
public class Scoreboard {

    private static Scoreboard instance;
    private Map<Plugin, HashMap<String, SimpleScoreboard>> scoreboards = new HashMap<Plugin, HashMap<String, SimpleScoreboard>>();
    private Map<Plugin, HashMap<String, BukkitTask>> timers = new HashMap<Plugin, HashMap<String, BukkitTask>>();

    private Scoreboard() {
    }

    /**
     * Get the Scoreboard instance
     *
     * @return Scoreboard instance
     */
    public static Scoreboard getInstance() {
        if (instance == null) {
            return (instance = new Scoreboard());
        }
        return instance;
    }

    /**
     * Create a new Scoreboard with the given title
     *
     * @param plugin Plugin using the scoreboard
     * @param name   Name of the Scoreboard
     * @param title  Title of the Scoreoard
     * @return SimpleScoreboard instance of created scoreboard
     */
    public SimpleScoreboard createNew(Plugin plugin, String name, String title) {

        if (exists(plugin, name)) {
            try {
                throw new ScoreboardException("Scoreboard already exists!");
            } catch (ScoreboardException e) {
                e.printStackTrace();
            }
            return null;
        }
        SimpleScoreboard sb = new SimpleScoreboard(title);

        if (scoreboards.containsKey(plugin)) {
            scoreboards.get(plugin).put(name, sb);
        } else {
            HashMap<String, SimpleScoreboard> map = new HashMap<String, SimpleScoreboard>();
            map.put(name, sb);
            scoreboards.put(plugin, map);
        }

        Logger.log("&bCreated new &fScoreboard &bfor plugin: " + plugin.getDescription().getName(), Logger.Type.INFO, true);
        return sb;
    }

    /**
     * Get the SimpleScoreboard instance for the given scoreboard
     *
     * @param plugin Plugin the Scoreboard is for
     * @param name   Name of the Scoreboard
     * @return SimpleScoreboard instance if exists, null otherwise;
     */
    public SimpleScoreboard getScoreboard(Plugin plugin, String name) {
        if (scoreboards.containsKey(plugin) && scoreboards.get(plugin).containsKey(name)) {
            return scoreboards.get(plugin).get(name);
        }
        return null;
    }

    /**
     * Get the Scoreboard Object itself
     *
     * @param plugin Plugin the Scoreboard is for
     * @param name   Name of the Scoreboard
     * @return Scoreboard object of given Scoreboard
     */
    public org.bukkit.scoreboard.Scoreboard getBuiltScoreboard(Plugin plugin, String name) {
        if (exists(plugin, name)) {
            scoreboards.get(plugin).get(name).build();
            return scoreboards.get(plugin).get(name).getScoreboard();
        }
        return Bukkit.getScoreboardManager().getNewScoreboard();
    }

    /**
     * Remove a Scoreboard
     *
     * @param plugin Plugin the Scoreboard is for
     * @param name   Name of the Scoreboard
     * @return true if Scoreboard was removed, false otherwise
     */
    public boolean removeScoreboard(Plugin plugin, String name) {
        if (exists(plugin, name)) {
            scoreboards.get(plugin).remove(name);
            return true;
        } else {
            return false;
        }
    }

    /**
     * Check whether or not a Scoreboard already exists
     *
     * @param plugin Plugin the Scoreboard is for
     * @param name   Name of the Scoreboard
     * @return true if exists, false otherwise
     */
    public boolean exists(Plugin plugin, String name) {
        return scoreboards.containsKey(plugin) && scoreboards.get(plugin).containsKey(name);
    }

    /**
     * Send the specified Scoreboard to the Specified Player
     *
     * @param plugin Plugin the Scoreboard is for
     * @param name   Name of the Scoreboard
     * @param p      Player to send the Scoreboard to
     */
    public void send(Plugin plugin, String name, Player p) {
        if (exists(plugin, name)) {
            if (p != null) {
                p.setScoreboard(getBuiltScoreboard(plugin, name));
            }
        }
    }

    /**
     * Send specified Scoreboard to everyone on the Server
     *
     * @param plugin Plugin the Scoreboard is for
     * @param name   Name of the Scoreboard
     */
    public void broadcast(Plugin plugin, String name) {
        if (exists(plugin, name)) {
            for (Player p : Bukkit.getOnlinePlayers()) {
                p.setScoreboard(getBuiltScoreboard(plugin, name));
            }
        }
    }

    /**
     * Start a Task for the given Scoreboard with the specified refresh time
     *
     * @param plugin      Plugin the Scoreboard is for
     * @param name        Name of the Scoreboard
     * @param refreshTime Refresh Interval (in seconds)
     *
     * @deprecated Not a very ideal way of doing this, recommended to create your own tasks.
     */
    public void startTimerForScoreboard(final Plugin plugin, final String name, final int refreshTime) {

        if (!exists(plugin, name)) return;

        long time = refreshTime * 20;

        if (timers.containsKey(plugin)) {
            timers.get(plugin).put(name, new BukkitRunnable() {
                @Override
                public void run() {
                    for (Player p : Bukkit.getOnlinePlayers()) {
                        p.setScoreboard(getBuiltScoreboard(plugin, name));
                    }
                }
            }.runTaskTimerAsynchronously(AuroraCore.getInstance(), 0, time));
        } else {
            HashMap<String, BukkitTask> task = new HashMap<String, BukkitTask>();
            task.put(name, new BukkitRunnable() {
                @Override
                public void run() {
                    for (Player p : Bukkit.getOnlinePlayers()) {
                        p.setScoreboard(getBuiltScoreboard(plugin, name));
                    }
                }
            }.runTaskTimerAsynchronously(AuroraCore.getInstance(), 0, time));

            timers.put(plugin, task);
        }
        Logger.log("&bTimer started for Scoreboard '&f"+name+"&b' from plugin '&f"+plugin.getDescription().getName()+"&b'", Logger.Type.INFO, true);
    }

    /**
     * Stop a specified Scoreboard Task
     *
     * @param plugin Plugin the Scoreboard is for
     * @param name   Name of the Scoreboard
     *
     * Not a very ideal way of doing this, recommended to create your own tasks.
     */
    public void stopTimerForScoreboard(Plugin plugin, String name) {
        if ((!timers.containsKey(plugin)) || (!timers.get(plugin).containsKey(name))) return;

        timers.get(plugin).get(name).cancel();
        timers.get(plugin).remove(name);
        Logger.log("&bTimer stopped for Scoreboard '&f"+name+"&b' from plugin '&f"+plugin.getDescription().getName()+"&b'", Logger.Type.INFO, true);
    }

    /**
     * Get the Tasks Map
     *
     * @return Tasks Map
     */
    public Map<Plugin, HashMap<String, BukkitTask>> getTimers() {
        return timers;
    }

    /**
     * Get the Scoreoards Map
     *
     * @return Scoreboards Map.
     */
    public Map<Plugin, HashMap<String, SimpleScoreboard>> getScoreboards() {
        return scoreboards;
    }
}
