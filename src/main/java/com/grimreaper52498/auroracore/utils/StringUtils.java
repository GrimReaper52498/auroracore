package com.grimreaper52498.auroracore.utils;

import com.grimreaper52498.auroracore.AuroraCore;
import me.clip.placeholderapi.PlaceholderAPI;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import java.util.Random;

/**
 * Created by GrimReaper52498 on 4/21/2016.
 *
 * @author GrimReaper52498 (Tyler Brady)
 */
public class StringUtils {

    /**
     * Color a message using & color codes
     *
     * @param message message
     * @return colored message
     */
    public static String color(String message) {
        return ChatColor.translateAlternateColorCodes('&', message);
    }

    /**
     * Replace placeholders for PlaceholderAPI if the server has it
     *
     * @param player  player to replace for
     * @param message message to replace placeholders in
     * @return message with replaced placeholders
     */
    public static String replace(Player player, String message) {
        if (AuroraCore.getInstance().hasPlaceholderAPI) {
            return PlaceholderAPI.setPlaceholders(player, message);
        }
        return message;
    }

    /**
     * Replace placeholders and color a message
     *
     * @param player  player to replace placeholders for
     * @param message message
     * @return proper message
     */
    public static String colorAndReplace(Player player, String message) {
        message = replace(player, message);
        return color(message);
    }


    /**
     * Rainbowize the given string
     *
     * @param string string to rainbowize
     * @return Rainbowized String
     */
    public static String rainbowizeString(String string) {
        int lastColor = 0;
        int currColor = 0;
        String newMessage = "";
        String colors = "4c6e2ab319d5f780";
        for (int i = 0; i < string.length(); i++) {
            do {
                currColor = new Random().nextInt(colors.length() - 1) + 1;
            }
            while (currColor == lastColor);

            newMessage += ChatColor.RESET.toString() + ChatColor.getByChar(colors.charAt(currColor)) + "" + string.charAt(i);

        }
        return newMessage;
    }

}
