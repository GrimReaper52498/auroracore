package com.grimreaper52498.auroracore.utils.logger;

import com.grimreaper52498.auroracore.utils.StringUtils;
import org.bukkit.Bukkit;

/**
 * Created by GrimReaper52498 on 4/22/2016.
 *
 * @author GrimReaper52498 (Tyler Brady)
 */
public class Logger {
    private static String prefix = StringUtils.color("&7&l[&b&lAurora&f&lCore&7&l] &r");

    public static void log(String message, Type type, boolean prefix) {
        Bukkit.getConsoleSender().sendMessage(StringUtils.color((prefix ? Logger.prefix : "") + (getTypePrefix(type)) + message));
    }

    /**
     * Log a message using the given prefix and type
     *
     * @param prefix  Prefix to use
     * @param message Message to log to console
     * @param type    Type of message
     */
    public static void log(String prefix, String message, Type type) {
        Bukkit.getConsoleSender().sendMessage(StringUtils.color((prefix == null ? "" : prefix) + (getTypePrefix(type) + message)));
    }

    private static String getTypePrefix(Type type) {
        String prefix = "";
        switch (type) {
            case NORMAL:
                prefix = "";
                break;
            case INFO:
                prefix = "&7&l[&b&lINFO&7&l] &r";
                break;
            case WARNING:
                prefix = "&7&l[&c&lWARNING&7&l] &r";
                break;
            case SEVERE:
                prefix = "&7&l[&4&lSEVERE&7&l] &r";
                break;
            case ERROR:
                prefix = "&7&l[&4&lERROR&7&l] &r";
                break;
        }
        return prefix;
    }

    public enum Type {
        NORMAL, INFO, WARNING, SEVERE, ERROR
    }
}
