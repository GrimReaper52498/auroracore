package com.grimreaper52498.auroracore.utils;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by GrimReaper52498 on 4/26/2016.
 *
 * @author GrimReaper52498 (Tyler Brady)
 */
public class TimeUtils {

    /**
     * Get the current Server time in dd-MM-yyyy HH:mm:ss a, z
     *
     * @return Current server time formatted
     */
    public static String getCurrent() {
        Date now = new Date();
        SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss a, z");
        return format.format(now);
    }
}
