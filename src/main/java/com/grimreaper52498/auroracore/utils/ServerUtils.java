package com.grimreaper52498.auroracore.utils;

import org.bukkit.Bukkit;

/**
 * Created by GrimReaper52498 on 4/22/2016.
 *
 * @author GrimReaper52498 (Tyler Brady)
 */
public class ServerUtils {

    public static boolean isVersion(String version) {
        String ver = getServerVersion();
        if (ver.equals(version)) {
            return true;
        } else if (ver.startsWith(version)) {
            return true;
        }
        return false;
    }

    public static String getServerVersion(){
        return Bukkit.getVersion().replaceAll(".+ \\(MC: ", "").replace(")", "");
    }

    public static boolean isPointEight(){
        return isVersion("1.8");
    }
    public static boolean isPointNine(){
        return isVersion("1.9");
    }
}
