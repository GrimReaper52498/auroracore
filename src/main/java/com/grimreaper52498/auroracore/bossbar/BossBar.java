package com.grimreaper52498.auroracore.bossbar;

import com.grimreaper52498.auroracore.utils.StringUtils;
import org.bukkit.Bukkit;
import org.bukkit.boss.BarColor;
import org.bukkit.boss.BarFlag;
import org.bukkit.boss.BarStyle;
import org.bukkit.entity.Player;

import java.util.List;

/**
 * Class to easily create and send a BossBarAPI to one or more players on the server
 *
 * @author GrimReaper52498 (Tyler Brady)
 */
public class BossBar {

    private org.bukkit.boss.BossBar bar;

    /**
     * Create a new Bossbar
     *
     * @param title Text on BossBarAPI
     */
    public BossBar(String title) {
        title = StringUtils.color(title);
        bar = Bukkit.createBossBar(title, BarColor.PINK, BarStyle.SOLID, BarFlag.PLAY_BOSS_MUSIC);
    }

    /**
     * Get the BossBarAPI Object
     *
     * @return the BossBarAPI Object
     */
    public org.bukkit.boss.BossBar getBar() {
        return bar;
    }

    /**
     * Add a flag to the BossBarAPI
     *
     * @param flag flag to add
     * @return this instance
     */
    public BossBar addFlag(BarFlag flag) {
        bar.addFlag(flag);
        return this;
    }

    /**
     * Remove flag from the BossBarAPI
     *
     * @param flag flag to remove
     * @return this instance
     */
    public BossBar removeFlag(BarFlag flag) {
        if (bar.hasFlag(flag)) {
            bar.removeFlag(flag);
        }
        return this;
    }

    /**
     * Get the progress of the BossBarAPI
     *
     * @return progress
     */
    public double getProgress() {
        return bar.getProgress();
    }

    /**
     * Set the progress of the BossBarAPI
     *
     * @param progress progress
     * @return this instance
     */
    public BossBar setProgress(double progress) {
        bar.setProgress(progress);
        return this;
    }

    /**
     * Get the color of the BossBarAPI
     *
     * @return color
     */
    public BarColor getColor() {
        return bar.getColor();
    }

    /**
     * Set the color of the BossBarAPI
     *
     * @param color color to set
     * @return this instance
     */
    public BossBar setColor(BarColor color) {
        bar.setColor(color);
        return this;
    }

    /**
     * Get the style of the BossBarAPI
     *
     * @return style
     */
    public BarStyle getStyle() {
        return bar.getStyle();
    }

    /**
     * Set the style of the BossBarAPI
     *
     * @param style style to set
     * @return this instance
     */
    public BossBar setStyle(BarStyle style) {
        bar.setStyle(style);
        return this;
    }

    /**
     * Get the current text on the BossBarAPI
     *
     * @return text
     */
    public String getTitle() {
        return bar.getTitle();
    }

    /**
     * Set the text of the BossBarAPI
     *
     * @param title text to display
     * @return this instance
     */
    public BossBar setTitle(String title) {
        bar.setTitle(title);
        return this;
    }

    /**
     * Get whether or not the BossBarAPI is currently visible
     *
     * @return true if visible, false otherwise
     */
    public boolean isVisible() {
        return bar.isVisible();
    }

    /**
     * Set the visibilty of the BossBarAPI
     *
     * @param val visibility
     * @return this instance
     */
    public BossBar setVisible(boolean val) {
        bar.setVisible(val);
        return this;
    }

    /**
     * Get the current players the BossBarAPI is being displayed for
     *
     * @return list of players viewing BossBarAPI
     */
    public List<Player> getPlayers() {
        return bar.getPlayers();
    }

    /**
     * Remove the BossBarAPI for the player
     *
     * @param p player to remove
     */
    public void clear(Player p) {
        bar.removePlayer(p);
    }

    /**
     * Display the BossBarAPI for given Player
     *
     * @param p player to display BossBarAPI for
     */
    public void send(Player p) {
        bar.addPlayer(p);
    }

    /**
     * send(Player) for every Player on the server
     */
    public void broadcast() {
        for (Player p : Bukkit.getOnlinePlayers()) {
            send(p);
        }
    }
}
