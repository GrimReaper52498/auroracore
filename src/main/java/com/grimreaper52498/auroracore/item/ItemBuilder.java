package com.grimreaper52498.auroracore.item;

import com.grimreaper52498.auroracore.enchants.Glow;
import com.grimreaper52498.auroracore.utils.StringUtils;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;
import org.bukkit.material.MaterialData;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by GrimReaper52498 on 4/26/2016.
 *
 * @author GrimReaper52498 (Tyler Brady)
 */
public class ItemBuilder {

    public Material material;
    public ItemStack is;
    public ItemMeta im;

    public ItemBuilder() {

    }

    public ItemBuilder(Material m) {
        material = m;
        this.is = new ItemStack(m);
        this.im = this.is.getItemMeta();
    }

    public ItemBuilder(ItemStack is) {
        this.is = is;
        this.im = this.is.getItemMeta();
    }

    public ItemBuilder(Material m, String name, ArrayList<String> lore) {
        this.is = new ItemStack(m);
        this.im = is.getItemMeta();
        im.setDisplayName(StringUtils.color(name));
        if (lore != null) {
            this.im.setLore(lore);
        }

    }

    public ItemBuilder(ItemStack m, String name, ArrayList<String> lore) {
        this.is = m;
        this.im = is.getItemMeta();
        im.setDisplayName(StringUtils.color(name));
        if (lore != null) {
            this.im.setLore(lore);
        }

    }

    public ItemBuilder(ItemStack m, String name) {
        this.is = m;
        this.im = is.getItemMeta();
        im.setDisplayName(StringUtils.color(name));
    }

    public ItemBuilder(Material m, String name) {
        this.is = new ItemStack(m);
        this.im = is.getItemMeta();
        im.setDisplayName(StringUtils.color(name));
    }

    public void setDisplayName(String s) {
        if (im == null) {
            return;
        }
        s = ChatColor.translateAlternateColorCodes('&', s);
        im.setDisplayName(s);
    }

    public void setLore(ArrayList<String> lore) {
        if (im == null) {
            return;
        }
        im.setLore(lore);
    }

    public void setLore(String... strings) {
        if (im == null) {
            return;
        }
        List<String> list = new ArrayList<String>();
        for (String s : strings) {
            list.add(ChatColor.translateAlternateColorCodes('&', s));
        }
    }

    public void setAmount(int i) {
        is.setAmount(i);
    }

    public void createHead(String s, String name) {
        this.is = new ItemStack(Material.SKULL_ITEM, 1, (byte) 3);
        SkullMeta sm = (SkullMeta) is.getItemMeta();
        sm.setOwner(s);
        sm.setDisplayName(ChatColor.translateAlternateColorCodes('&', name));
        is.setItemMeta(sm);
    }

    public void addEnchantment(Enchantment e, int multiplier) {
        if (im == null) {
            return;
        }
        im.addEnchant(e, multiplier, true);
    }

    public void addGlow() {
        Glow.addGlow(this.is);
    }

    public void setDurability(short s) {
        is.setDurability(s);
    }

    public void setData(MaterialData data) {
        is.setData(data);
    }

    public void setItemMeta(ItemMeta im) {
        this.im = im;
        is.setItemMeta(im);
    }

    public ItemStack getItem() {
        if (is.getType() == Material.SKULL_ITEM) {
            return is;
        } else {
            is.setItemMeta(im);
            return is;
        }
    }
}