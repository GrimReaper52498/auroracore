package com.grimreaper52498.auroracore;

import com.grimreaper52498.auroracore.bossbar.BossBar;
import com.grimreaper52498.auroracore.cache.PlayerCaches;
import com.grimreaper52498.auroracore.config.Config;
import com.grimreaper52498.auroracore.player.AuroraPlayer;
import com.grimreaper52498.auroracore.scoreboard.Scoreboard;
import com.grimreaper52498.auroracore.scoreboard.SimpleScoreboard;
import com.grimreaper52498.auroracore.utils.ServerUtils;
import com.grimreaper52498.auroracore.utils.TimeUtils;
import com.grimreaper52498.auroracore.utils.logger.FileLogger;
import com.grimreaper52498.auroracore.utils.logger.Logger;
import org.bukkit.Bukkit;
import org.bukkit.boss.BarColor;
import org.bukkit.boss.BarFlag;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;

import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.text.DecimalFormat;
import java.util.HashMap;

/**
 * Created by GrimReaper52498 on 4/21/2016.
 *
 * @author GrimReaper52498 (Tyler Brady)
 */
public class AuroraCore extends JavaPlugin implements CommandExecutor {

    private static AuroraCore instance;
    private static double tps = 0;
    private static HashMap<JavaPlugin, HashMap<String, FileLogger>> loggers = new HashMap<>();
    public boolean hasPlaceholderAPI;
    private BukkitTask tpsTask;

    public static AuroraCore getInstance() {
        return instance;
    }

    /**
     * Get the TPS of the Server
     *
     * @return TPS of server
     */
    public static double getServerTPS() {
        DecimalFormat format = new DecimalFormat("##.##");

        return Double.parseDouble(format.format(tps));
    }

    /**
     * Get the AuroraPlayer object for the specified player
     *
     * @param player Player to get for
     * @return AuroraPlayer object for player
     */
    public static AuroraPlayer getPlayer(Player player) {
        if (PlayerCaches.getPlayers().containsKey(player)) {
            return PlayerCaches.getPlayers().get(player);
        } else {
            AuroraPlayer ap = new AuroraPlayer(player);
            PlayerCaches.getPlayers().put(player, ap);
            return ap;
        }
    }

    /**
     * Get the current version of the server in x.x.x format (example: 1.9.2)
     *
     * @return version of server
     */
    public static String getServerVersion() {
        return ServerUtils.getServerVersion();
    }

    /**
     * Get AuroraCores Scoreboard Manager
     *
     * @return Scoreboard Instance
     */
    public static Scoreboard getScoreboardManager() {
        return Scoreboard.getInstance();
    }

    /**
     * Create a new BossBar with the given title (1.9+ ONLY)
     *
     * @param title title for BossBarAPI
     * @return BossBar Object with given title
     */
    public static BossBar createNewBossBar(String title) {
        return new BossBar(title);
    }


    /**
     * Get the Config Manager
     *
     * @return Config Instane
     */
    public static Config getConfigManager() {
        return Config.getInstance();
    }

    /**
     * Log a given message with the given prefix and type
     *
     * @param prefix  Prefix to use
     * @param message Message to send
     * @param type    Type of log
     */
    public static void log(String prefix, String message, Logger.Type type) {
        Logger.log(prefix, message, type);
    }

    /**
     * Get a epecified log file
     *
     * @param plugin Plugin the file belongs to
     * @param id     ID of the log file
     * @param file   File object
     * @return FileLogger for the given parameters
     */
    public static FileLogger getFileLog(JavaPlugin plugin, String id, File file) {
        if (!loggers.containsKey(plugin)) {
            HashMap<String, FileLogger> map = new HashMap<>();
            loggers.put(plugin, map);
        }
        if (!loggers.get(plugin).containsKey(id)) {
            loggers.get(plugin).put(id, new FileLogger(file));
        }
        return loggers.get(plugin).get(id);
    }

    @Override
    public void onEnable() {
        instance = this;
        super.onEnable();

        hasPlaceholderAPI = Bukkit.getPluginManager().isPluginEnabled("PlaceholderAPI");

        if (!getDataFolder().exists()) {
            getDataFolder().mkdirs();
        }

        startTPSTask();

        FileConfiguration config = Config.getInstance().getConfig(this, this.getDataFolder(), "test");
        config.set("test", true);
        Config.getInstance().saveConfig(this, this.getDataFolder(), "test");
        FileConfiguration config2 = Config.getInstance().getConfig(this, new File("test"), "test");
        config2.set("test", true);
        Config.getInstance().saveConfig(this, new File("test"), "test");

        FileLogger logger = getFileLog(this, "test", new File(getDataFolder(), "log.txt"));
        logger.load();
        logger.setAutoSave(true);
        logger.addLine(TimeUtils.getCurrent() + " This is a test log");
        logger.save();
    }

    @Override
    public void onDisable() {
        super.onDisable();

        stopAllTasks();
    }

    private void stopAllTasks() {
        Logger.log("&bCancelling all tasks...", Logger.Type.INFO, true);
        for (Plugin plugin : Scoreboard.getInstance().getTimers().keySet()) {
            for (String name : Scoreboard.getInstance().getTimers().get(plugin).keySet()) {
                Scoreboard.getInstance().getTimers().get(plugin).get(name).cancel();
                Scoreboard.getInstance().getTimers().get(plugin).remove(name);
                Logger.log("&bRemoved task for Scoreboard '&f" + name + "&b' from plugin '&f" + plugin.getDescription().getName() + "&b'", Logger.Type.INFO, true);
            }
        }

        stopTPSTask();
        Logger.log("&bTasks cancelled...", Logger.Type.INFO, true);
    }

    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (cmd.getName().equalsIgnoreCase("test")) {
            if (sender instanceof ConsoleCommandSender) {
                if (args.length == 1) {
                    if (args[0].equalsIgnoreCase("tps")) {
                        Logger.log("&b&lTPS: &f&l" + getServerTPS(), Logger.Type.NORMAL, true);
                    }
                    if (args[0].equalsIgnoreCase("version")) {
                        Logger.log("&b&lVersion: &f&l" + getServerVersion(), Logger.Type.NORMAL, true);
                    }
                }
                return false;
            }
            AuroraPlayer ap = getPlayer((Player) sender);
            if (args.length == 1) {
                if (args[0].equalsIgnoreCase("startscoreboardtask")) {

                    SimpleScoreboard sb = getScoreboardManager().createNew(this, "Test", "&d&b&lTest&f&lScoreboard");
                    sb.add("                                     ");
                    try {
                        sb.add("Ping: " + ap.getPing());
                    } catch (ClassNotFoundException e) {
                        e.printStackTrace();
                    } catch (NoSuchMethodException e) {
                        e.printStackTrace();
                    } catch (InvocationTargetException e) {
                        e.printStackTrace();
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    } catch (NoSuchFieldException e) {
                        e.printStackTrace();
                    }

                    sb.blankLine();

                    sb.add("TPS: " + getServerTPS());

                    sb.blankLine();

                    sb.add("Version: " + getServerVersion());

                    getScoreboardManager().startTimerForScoreboard(this, "Test", 1);
                    return true;
                }
                if (args[0].equalsIgnoreCase("tps")) {
                    ap.sendMessage("&b&lTPS: &f&l" + getServerTPS());
                    return true;
                }
                if (args[0].equalsIgnoreCase("ping")) {
                    try {
                        ap.sendMessage("&b&lPING: &f&l" + ap.getPing());
                    } catch (ClassNotFoundException e) {
                        e.printStackTrace();
                    } catch (NoSuchMethodException e) {
                        e.printStackTrace();
                    } catch (InvocationTargetException e) {
                        e.printStackTrace();
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    } catch (NoSuchFieldException e) {
                        e.printStackTrace();
                    }
                    return true;
                }
                if (args[0].equalsIgnoreCase("version")) {
                    ap.sendMessage("&b&lSERVER VERSION: &f&l" + getServerVersion());
                }
            }
            createNewBossBar("&bThis is a test BossBarAPI").setColor(BarColor.WHITE).addFlag(BarFlag.PLAY_BOSS_MUSIC).setProgress(0.5).broadcast();

            ap.setTabHeaderFooter("&b&lThis is a test header", "&b&lThis is a test footer");
            ap.sendActionbarMessage("&b&lThis is a test actionbar message");
            ap.sendTitleAndSubtitle("&b&lTest Title", "&f&lTest Subtitle");

        }
        return false;
    }

    private void startTPSTask() {
        if (tpsTask != null) {
            tpsTask.cancel();
        }
        tpsTask = new BukkitRunnable() {
            long sec;
            long currentSec;
            double ticks;
            int delay;

            @Override
            public void run() {
                sec = (System.currentTimeMillis() / 1000);
                if (currentSec == sec) {
                    ticks++;
                } else {
                    currentSec = sec;
                    tps = (tps == 0 ? ticks : ((tps + ticks) / 2)) + 1;
                    if (tps >= 20.0) {
                        tps = 20;
                    }
                    ticks = 0;
                    if ((++delay % 300) == 0) {
                        delay = 0;
                    }
                }
            }
        }.runTaskTimer(this, 0, 1);

        Logger.log("&bStarted the TPS task.", Logger.Type.INFO, true);
    }

    private void stopTPSTask() {
        if (tpsTask != null) {
            tpsTask.cancel();
            tpsTask = null;
        }
        Logger.log("&bStopped TPS task.", Logger.Type.INFO, true);
    }

    /**
     * Check if the Minecraft server is of a current version.
     *
     * @param ver Version to check
     * @return true if versions match, false otherwise
     */
    public boolean compareVersions(String ver) {
        return ServerUtils.isVersion(ver);
    }
}
